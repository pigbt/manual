Evaluation of high-speed downlink
=================================

1. External PRBS7 sequence generator connected to RX inputs of lpGBT

 * Click on the menu bar in the top left corner of the screen and select test Core
 * Select RX chip mode

2. Equalizer settings and eye diagram measurement

 * Click on the menu bar in the top left corner of the screen and select high-speed
 * Select attenuation value, programmable attenuator can provide attenuation of 0 dB, -3.6 dB or -9.5 dB
 * Select a value for the capacitance 
 * Select a value for resistors 1 to 4  

3. Run a bit error test

 * Click on the menu bar in the top left corner of the screen and select test features
 * In BERT Pattern Checker select data source to be checked
 * Select reference channel signal for pattern checker 
 * Set the duration of the measurement 
 * Start BERT measurement
 * Read BERT result
