Connect to PiGBT Wired network 
------------------------------


**Slave DHCP mode** (Default mode)

In this mode, the first thing to do is to plug your raspberry pi to your internet router using a RJ45 cable in order to connect to the network and obtain an IP address. PiGBT and its resources can be accessed from any device connected to the same network. The IP address given to the Raspberry Pi is displayed on the LCD.

.. _img1_PigbtDhcpSlave:
.. figure:: ../img/pigbt_dhcp_slave.jpg
   :scale: 20 %
   :align: center

   Ethernet information display

#. Set the Master Mode switch to OFF on the translator board.
#. Connect the provided ribbon cable between the translator board and the VLDB+.
#. Read the raspberry pi Ethernet IP displayed on the LCD
#. Connect your device to the same network
#. Read the raspberry pi Ethernet IP address displayed on the LCD.
#. Open a browser and enter your PiGBT url: http://*ip_address*:8080.

Then, the Raspberry password must be changed to avoid security vulnerabilities. Connect to the Raspberry through ssh and do:

`sudo passwd pi`

Then change the password.

.. note::
   If you received your Control Toolkit after the 10/2024, a custom Raspberry Pi password was set and provided.

**Master DHCP mode** 

In this mode, Raspberry will distribute new IP addresses (DHCP server in Raspberry Pi).

.. warning::
   To activate this mode, the PiGBT Wi-Fi mode has to be disabled.

#. Switch off the raspberry pi.
#. Set the Wi-Fi switch to OFF on the translator board.
#. Set the Master Mode switch to ON on the translator board.
#. Connect the provided ribbon cable between the translator board and the VLDB+.
#. Connect RJ45 cable between your computer and the Raspberry Pi Ethernet port.
#. Read the raspberry pi Ethernet IP address displayed on the LCD.
#. Open a browser and enter your PiGBT url: http://*ip_address*:8080.




