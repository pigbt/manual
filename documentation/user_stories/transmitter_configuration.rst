Transmitter configuration
=========================

1. Set transmitter mode at 10Gbps data rate and FEC5 encoding

  *  Click on the menu bar in the top left corner of the screen and select Core
  *  Select TX chip mode button then 10Gbps and FEC5 button

2. Enable all uplink elinks with data rate at 1280Mbps auto phase selection and enabled termination

  *   Select 1280 data rate button 
  *   Auto phase selection is set by default for all uplinks
  *   Select TERM button to activate termination
