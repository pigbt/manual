Raspberry Pi standalone setup
-----------------------------


Raspberry Pi is a single board compter. In this mode connect a screen and mouse to the Raspberry Pi and use it as a standalone system.

#. Connect your mouse and keyboard to the USB ports at the bottom of the Raspberry Pi USB connectors.
#. Plug a micro HDMI cable this the micro HDMI port next to the USB-C connector (labeled HDMI0 on the PCB).
#. Connect ribbon cable between the translator board and the VLDB+.
#. Power up the Raspberry Pi and wait for it to boot.
#. On the Raspberry Pi Raspbian OS interface open a Browser: menu/internet/chromium web browser.
#. Open a browser and enter the PiGBT url: http://*ip_address*:8080
