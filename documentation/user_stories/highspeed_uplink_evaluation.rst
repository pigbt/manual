Evaluation of high-speed uplink
===============================

1. Configure lpGBT in external 40 MHz reference clock

 * Set lock mode switch to External lock mode on the VLDB+ board

2. Configuration PRBS7 in the serializer

 * Click on the menu bar in the top left corner of the screen and select test features
 * Select PRBS7 generator in Uplink Serializer Data Source

3. Modulation current and pre-emphasis monitoring for line driver

 * Click on the menu bar in the top left corner of the screen and select high-speed
 * Select modulation current slider to monitor the current in Line driver section
 * To activate pre-emphasis driver click on Enable
 * Select pulse width of pre-emphasis: Long (60 ps) or Short (40 ps)
