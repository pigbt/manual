Clocks evaluation
=================

**Programmable clocks evaluation in receiver mode**

1. Set receiver mode 

  *  Click on the button in the left corner of the navigation bar and select core
  *  Select RX chip mode button in Chip mode section

2. Select two phase shifter channels one with fine phase shifting

  *  Click on the button in the left corner of the navigation bar and select clocks
  *  Select a frequency button to activate channels
  *  Set Enable in Fine Tune to enable fine phase shifting for one phase shifter (resolution = 48.8 ps)

3. Program different options the phase shift of the channels

  *  Click on phase slider to program different options for the phase shifter
  *  Changes can be observed with a scope
  *  Click on the Settings button to enabled advanced configuration options  
  *  Click on drive strength to select an amplitude
  *  If phase control tune button is disabled you will program coarsely the phase for the channel (resolution = 781.25 ps)
  *  Fine phase setting is disabled by default
