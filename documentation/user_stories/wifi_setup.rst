Connect to PiGBT Wi-Fi 
----------------------

By default, the PiGBT Wi-Fi is enabled. After powering-up the Raspberry Pi, You should see a “PIGBT_XXXX” network in the list of accessible networks.
Once the network is visible, all you have to do is connect to it. The network SSID and the default password are displayed on the LCD.

.. _img0_PigbtWifiInfo:
.. figure:: ../img/pigbt_wifi.png
   :scale: 60 %
   :align: center

   Wi-Fi network information display

.. note::
   You can change the network SSID and regenerate a new password. Keep the OFF button on the translator board pressed for 5 seconds.

#. Set the Wi-Fi switch to ON on the translator board.
#. Connect  the provided ribbon cable between the translator board and the VLDB+.
#. Check the WiFi SSID, the IP address and the password displayed on the LCD screen
#. Connect to the Raspberry Pi WiFi network with a device.
#. Open a browser and enter your PiGBT url: http://121.224.4.10:8080 (example ip taken from the image)

.. warning::
   To activate this mode, the DHCP master mode has to be disabled. Switch off the raspberry pi and set the DHCP master mode switch to disable. Wait for it to boot.
