PLL performance evaluation
==========================

**Evaluation of the PLL performance**

1. Configure lpGBT in external 40 MHz reference clock

 * Set lockmode switch to 0 on LpGBT characterization board 

2. Configure lpGBT in simple TX mode

 * Click on menu bar in the top left corner of the screen and select Core
 * Select TX chip mode
 * Select transmitter mode data rate and FEC encoding

3. Configure 1 eclock @320 MHz, 2 mA drive strength and no pre-emphasis 

 * Click on the menu bar in the top left corner of the screen and select Clocks
 * For EPCLKn select 320 MHz frequency button
 * Click on setting button to access to the advanced configuration (not implemented)
 * Select drive strength 2.0 mA amplitude
 * Select disable pre-emphasis mode

4. Change PLL parameters(charge pump current and resistors values) (not implemented)

 * In PLL configuration division select PLL charge pump current to set current value
 * Select PLL resistor value
