Analog peripherals configuration
================================

1. Enable internal voltage reference 

  *  Click on the menu bar in the top left corner of the screen and select analog
  *  Select Generator Enable button
  *  Select slider to tune voltage value

2. Set Voltage DAC to 0.63 V

  *  In Voltage DAC section select Enable 
  *  Click on voltage DAC value slider and set voltage (Vout=(VOLDACValue/4096)*vref)

3. Monitor VDD inside the chip

  *  In ADC section clock on VDD monitoring
  *  Select VDD source in ADC positive input selector
  *  Click on start conversion button to read ADC value

3. Read back temperature sensor

  *  Go to ADC Temperature Sensor section 
  *  Click on Measure Temperature button to start internal sensor temperature measurement
  *  Read the code from the temperature ADC.

4. Read resistance of PT1000 sensor connected to ADCn channel

  *  In current DAC section, enable CDAC current
  *  Select current DAC slider and set current value
  *  Connect sensor to correct ADC pin
  *  In Voltage ADC section select ADC pin used in ADC positive input selector
  *  Read ADC value of PT1000 sensor resistance conversion

Analog peripherals configuration when calibrated
================================================

1. Select the type of calibration that you want

  *  Click on the menu bar in the top left corner of the screen and select analog
  *  Click on the "Chip-specific calibration values". If no calibration values are available for your chip, click on "Average calibration values"
  *  In case you want to stop using calibration values, click on "No calibration used"
  *  Depending on the selection, the Analog interface will change accordingly (calibrated/not calibrated)

2. Enable internal voltage reference when calibrated

  *  Select Generator Enable button
  *  Click on "click here to get ~1 V". The slider will automatically be set to the best tune value to be the closest possible to 1V (depending on calibration error).

3. Set Voltage DAC to 0.44 V

  *  In Voltage DAC section select Enable
  *  Click on the voltage DAC slider and release it when the Tune value is 0.44 V. Wait for calibration.
  *  The Voltage DAC will be set to the closest possible to 0.44 V (depending on calibration error).
  *  Read the Voltage DAC code value that was set in Byte value to know what code corresponds to 0.44 V.

4. Set CDAC current to 770 uA for ADC5 (only)

  *  In the Current DAC section select Enable
  *  In the Output Pin select the CDAC5
  *  Click on the CDAC slider and release it when the Tune value is 770 uA. Wait for calibration.
  *  When finished, you can read the CDAC code value that was set in Byte value to know what code corresponds to 770 uA.

5. Read ADC voltage value for ADC2

  *  In the ADC section select the as ADC Positive Input ADC2 and as ADC Negative Input VREF/2
  *  In case you expect to have a value close to 0.5 V, you can select gain x8 or gain x16. If not, select gain x2.
  *  Click on Start conversion. The result will be in voltage.
  *  You can also read the ADC code as a result in Byte value to know what code corresponds to the voltage read.

6. Read back temperature sensor

  *  Go to ADC Temperature Sensor section
  *  Click on Measure Temperature button to start internal sensor temperature measurement
  *  Read temperature from ADC is the sensor temperature in degree Celsius
  *  You can also read the ADC code value as a result in Byte value to know what code corresponds to the temperature read.

7. Monitor VDD inside the chip

  *  In ADC section clock on VDD monitoring
  *  Select VDD source in ADC input selector
  *  Click on start conversion button to read the voltage value
  *  You can also read the ADC code value as a result in Byte value to know what code corresponds to the internal voltage read.