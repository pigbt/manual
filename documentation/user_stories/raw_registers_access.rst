Access to raw values of registers 
=================================

  *  Click on the menu bar in the top left corner of the screen and select Registers
  *  On register map filter, enter register name or register address to read
  *  To modify register value, click on register name on the list and enter a new value
  *  Example: To disable the watchdog enter POWERUP0 in the Register map search bar
              Click on the register name in the list
              Set bit 7 PUSMpllWdogDisable to 1 in the Update Register Value window
