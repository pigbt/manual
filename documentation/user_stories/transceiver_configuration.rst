Transceiver configuration 
=========================

1. Set transceiver mode at 5Gbps  data rate and FEC12 encoding

  *  Click on the menu bar in the top left corner of the screen and select Core
  *  Select TRX chip mode button then 5Gps and FEC12 button

2. Enable all uplink elinks with data rate @320Mbps, auto phase selection and enabled termination

  *  Select 320 data rate button 
  *  Auto phase selection is set by default for all uplinks
  *  Select TERM button

3. Enable 4 downlinks elinks at 160Mbps with 2 mA amplitude and no pre-emphasis

  *  Select 160 data rate button
  *  Click on drive strength option and select 2.0 to set 2 mA amplitude
  *  Click on pre-emphasis option and select disable

4. Configure 2 eClocks at 320MHz with 2 mA amplitude and no pre-emphasis

  *  Click on the menu bar in the top left corner of the screen and select Clocks
  *  Go to eclocks number to configure then select 320 Frequency button
  *  Click on drive strength option and select 2.0 to set 2mA amplitude
  *  Click on pre-emphasis option and select disable

5. Configure 2 eClocks @320MHz delayed by 10ns with 2 mA amplitude and no pre-emphasis

  *  Go to eclocks number to configure then select 320 Frequency button
  *  Click on phase slider and move the cursor to set phase value at 10ns 
  *  Click on drive strength option and select 2.0 to set 2mA amplitude
  *  Click on pre-emphasis option and select disable

6. Enable RSTOUTB with 100 ns pulse duration (not implemented)

  *  Click on the menu bar in the top left corner of the screen and select Status
  *  Go to LpGBT reset out section
  *  Click on phase duration select option for rstoutb and select 100
  *  Click on Reset button to generate the pulse

7. Example of LpGBT I2CM1 communication with slave connected

  *  Click on the menu bar in the top left corner of the screen and select I2C Master number
  *  Enter slave address in hexadecimal 0x51
  *  Select 8-bit register address width
  *  Enter register address in hexadecimal 0x00
  *  Set Bus Speed to 400 kHz
  *  In Write division enter the data to write 0x01
  *  Click on the write button
  *  Repeat the process for each slave address register to configure

8. Check if downlink high speed is locked

  *  In the top right corner of the screen the spinner is green if downlink high-speed data are locked

9. Check if elinks receivers are locked and the phase selected

  *  Reloading the page will automatically read back the register value and set the page according to register configuration
  *  Click on menu bar and Status 
  *  Read status lock and current phase for EPRX group/channel (not implemented)
