***
FAQ
***

This is a list of Frequently Asked Questions about PiGBT.

.. _faq_q1:

How do I reinstall Raspberry OS on my SD card? 
----------------------------------------------

This `Raspberry OS <https://cernbox.cern.ch/s/bIklORqmHJFWpaT>`_ for the control toolkit is a custom image from the official Raspbian OS with additional libraries and scripts to it to get the PiGBT software working.

We recommend to use  Win32DiskImage to write images to SD cards. Download the Win32DiskImager utility from the `Sourceforge Project page <https://sourceforge.net/projects/win32diskimager/>`_. Run it to install the software.

How to setup ?

   - Insert the SD card into your SD card reader.
   - Run the Win32DiskImager utility from your desktop or menu.
   - Select the image file you downloaded.
   - In the device box, select the drive letter of the SD card. Make sure you select the correct drive: if you choose the wrong drive, you could    destroy the data on your computer's hard drive.
   - Click 'Write' and wait for the write to complete.
   - Exit the imager and eject the SD card.
   - Insert the SD card into your Raspberry PI

.. _faq_q2:

How do I troubleshoot piGBT as a result of a software update? 
-------------------------------------------------------------

If you have recently experienced unexpected behavior of the piGBT interface after a software upgrade, one solution is to delete your browser cache and cookies.
This will ensure you are viewing the most up-to-date version of the piGBT Web application.
This may also solve server error messages or problems you experience with the interface after an update.

.. _faq_q3:

How to find my Raspberry Pi IP address (no OLED) ?
--------------------------------------------------

If you have physical access to your Pi with a dedicated monitor connected, then the process is quite simple.
Your Raspberry Pi 4 is equipped with two HDMI micro ports, allowing you to connect monitors. To connect Raspberry Pi 4 to a screen, you need a micro HDMI to HDMI cable.

After starting your Raspberry Pi operating system, open a terminal in your office environment and follow this steps to get information about your network. 

   Option 1 - Ethernet LAN.

   To find the IP address assigned to eth0 and view it in the terminal, execute the following command:

   .. prompt:: bash
   $ hostname -I

   Option 2 - Wi-Fi connection.

   To get the ssid and password of your Raspberry Pi wireless access point, type the following commands into your device:

   .. prompt:: bash
   $ grep "ssid=PIGBT_" /etc/hostapd/hostapd.conf
   
   The result of the above command is the ssid name.

   .. prompt:: bash
   $ grep "wpa_passphrase=" /etc/hostapd/hostapd.conf

   The result of the above command is the password.

.. _faq_q4:

How to read back the commands sent to lpGBT using the piGBT web interface ?
---------------------------------------------------------------------------

When you press any button, you write the value to the lpGBT registers through I2C.

The read back is done during the loading of the page to initialize the components of the web page with the values corresponding to those read in the registers.

If a communication error occurs, a pop-up appears on the screen with the exception detected.

An alternative way to read the value would be to navigate to the registry page to display the value of the registers.


