.. _versionhistory:

================================================================================
Version History
================================================================================

`In order to see the updates, please remove the cookies or navigate the manual in incognito mode.`

.. _versionhistory_table:
.. table:: Version history.
   :align: center

   ============================== ============================== ======================================================================
    Date                           Author                         Description
   ============================== ============================== ======================================================================
    06/11/2024                    Daniel Hernandez                Add updates information in :numref:`how_to_update` in case of using proxy server 
    10/10/2024                    Daniel Hernandez                - Add download and update information for PiGBT V1.5.1 in :numref:`pigbt_version_history`
                                                                  - Add information in :numref:`wired_config` when setting the Control Toolkit in DHCP mode.
    01/07/2024                    Daniel Hernandez                Add download and update information for PiGBT V1.5 in :numref:`pigbt_version_history`
    27/03/2023                    Daniel Hernandez                Add download and update information for PiGBT V1.4 in :numref:`pigbt_version_history`
    20/03/2023                    Daniel Hernandez                - Add new :numref:`connection`
                                                                  - Add PiGBT versions and download in :numref:`pigbt_version_history`
                                                                  - Update :numref:`getting_started` with respect to the new connection naming
                                                                  - Update the introduction of :numref:`getting_started`
    03/02/2023                    Daniel Hernandez                - Update :numref:`startrpi` regarding "How to power the control toolkit?"
                                                                  - Update image OS to download in :numref:`faq_q1`
    2020 - 2023                   Nour Guettouche                 Release and maintenance
   ============================== ============================== ======================================================================