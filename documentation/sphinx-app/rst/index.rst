.. PIGBT Manual documentation master file, created by Nour GUETTOUCHE
   sphinx-quickstart on Thu Apr  2 18:52:00 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

VLDB+ control toolkit manual
============================

.. note::

   This user's guide contains information for the VLDB+ control toolkit including the hardware and software specifications.

The **Low-Power Gigabit Transceiver** (LpGBT) is a radiation tolerant ASIC for multipurpose high-speed bidirectional optical links in HEP experiments.
Its data interface to the detectors’ frontends is highly configurable supporting multiple data rates. The LpGBT is designed to be used for data acquisition as well as for trigger and timing distribution.
It also features control interfaces and environmental monitoring functions to implement experiment control.

Therefore, this device contains a huge register map to be configured and to be monitored in real-time.

The previous GBTx component configuration was made using a JAVA application and a custom USB to I2C dongle designed at CERN.

Nevertheless, that implied some limitations:

*  No remote control
*  Static interface
*  Custom electronics
*  OS compatibility
*  No GPIOs available

For these reasons, the LpGBT control toolkit based on **Raspberry Pi** was selected as the best candidate to be the new programmer system called **PiGBT**.

.. _img0_ControlToolkit:
.. figure:: ../img/control_toolkit.jpg
   :scale: 60 %
   :align: center

==================================================================================================================================================

.. toctree::
   :numbered:

   overview
   quick_start
   configuration
   exemple_features
   updates
   faq
   version_history
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
