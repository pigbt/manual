*********** 
Quick start
***********

.. _startrpi:

Starting Raspberry Pi
=====================

**How to power the control toolkit?**

The recommended way to power the Raspberry Pi 4 and the Translator Board is via the 2-pin connector ``J4`` located in the Translator Board.
Through this connector, a voltage between 5-42 V can be supplied. The recommended supply for the Kit is 10 V with a current limited to 1 A.

.. _img3_PowerSupplyMode1:
.. figure:: ../img/RPI_power_connector.png
   :scale: 70 %
   :align: center

   J4 Port (10 V @ 1A)

In case you are using a VLDB+ (10V) or any other hardware, it is highly recommended to power both Control Toolkit and device to control 
with the same power supply and interconnecting both grounds.

.. warning::
   The use of the USB-C port to power the Control Toolkit is no longer recommended as the device which the Kit is controlling at the end of the ribbon cable
   may not use the same ground source.

.. _wired_config:

Connecting PiGBT
================

Gunicorn is a Python Web Server Gateway Interface (WSGI) HTTP server. The server hosts the PiGBT Web page that connects to the Python program
which in turn is getting data from the LpGBT registers. To access PiGBT you have to connect your device to one of the three different PiGBT networks,
open a web browser, and enter the PiGBT URL: http://*network_ip_address*:8080. 


.. include:: user_stories/wifi_setup.rst
.. include:: user_stories/wired_configuration.rst
.. include:: user_stories/stand_alone_setup.rst
