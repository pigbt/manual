.. _pigbt_updates:

*************
PiGBT updates
*************

.. _how_to_update:

How to update
=============

When a new version of the PiGBT software is created, the Raspberry Pi will automatically update the application once the system is connected to the internet. Do not switch off your Raspberry Pi if you read the following message: on the LCD screen: “UPDATING PiGBT reloading files wait”. Please, wait around 1 minute until the system reboots.

If the PiGBT software update is interrupted, you have to make sure that you have the latest tag list from the remote repository. You can easily access the command line of your Raspberry Pi:

* Open a terminal on your computer and enter the ssh command: **ssh pi@[IP]**. The password is *raspberry*.
* Then go to *cd  /home/pi/pigbt* and check the latest tag from the remote repository : *git tag | tail -1*
* Compare it the with the current tag to make sure that the repository is up to date: *git describe --tags*
* In the case it's not up to date, enter the following command: *git checkout tags/ (latest tag version number)*
* Go to *cd /home/pi/pigbt/frontend* folder
* Run this command to update the PiGBT frontend software *npm run build -- --mode staging*
* Reboot the system *sudo reboot* 
* PiGBT is up to date.

In case you are using a proxy server in your Raspberry Pi and you have to manually open specific addresses to the network for the update, these are the only ones to open:

- https://gitlab.cern.ch/ since `git pull` and other git commands are executed to get the updates.
- https://lpgbt.web.cern.ch/ since `wget https://...` command is executed to get analog lpGBT calibration data.
- python pip since `pip install git+...` command is executed to get `lpgbt_control_lib` data to run the lpGBT libraries.

.. _pigbt_version_history:

PiGBT version history
=====================

`In order to see the updates, please remove the cookies or navigate the manual in incognito mode.`

.. _pigbtversions_table:
.. table:: Version history.
   :align: center

   ============================== ========================================================================== =====================================================================================================================================================
    Date                           Version and download                                                       Description
   ============================== ========================================================================== =====================================================================================================================================================
    11/10/2024                     `V1.5.1 <https://cernbox.cern.ch/s/dxkGO1SK1y6Xc53>`_                      - Add security check at initialization to keep CUPS service disabled. More info in `Ubuntu <https://ubuntu.com/blog/cups-remote-code-execution-vulnerability-fix-available>`_.
                                                                                                              - Put default password as `pigbtv1_5_1` from downloaded version (link on the left).
    01/07/2024                     `V1.5 <https://cernbox.cern.ch/s/lquR0ySCds7mMDS>`_                        - Update the lpgbt_control_lib to version 4.1.1.
                                                                                                              - Manage timeout for Downlink BERT (integrated in new `lpgbt_control_lib <https://gitlab.cern.ch/lpgbt/lpgbt_control_lib/-/blob/master/lpgbt_control_lib/lpgbt.py?ref_type=heads#L1380>`_).
                                                                                                              - Due to the excesive time required to do 32G and 8G BERT (software timeouts), these were removed.
                                                                                                              - Software timeout if piGBT blocked set from 30 s to 60 s.
                                                                                                              - Added spinner for BERT measurement.
                                                                                                              - Fixed uplink EC phase tracking which was inverted (continous vs static).
                                                                                                              - Removed I2C broadcast. PiGBT is now compatible with I2C multi-drop bus (the lpGBTs have to be of the same version). 
                                                                                                              - Added calibration excel file in Raspberry to use it for any lpGBT (average or specific calibration numbers).
                                                                                                              - Added calibration option for the Analog section.
                                                                                                              - Add PiGBT software version in back-end and front-end.
    27/03/2023                     `V1.4 <https://cernbox.cern.ch/s/VeBsewXd175Ef6d>`_                        - Update the lpgbt_control_lib to version 4.0.0.
                                                                                                              - Fixed the problem of the Control Toolkit APP LED turning red after doing an lpGBT Reset through the PiGBT.
                                                                                                              - Fixed the problem of the High-Speed -> Line-Driver -> Modulation Current and Pre-emphasis Current sliders not setting good values.
                                                                                                              - Moved Test Outputs section above Registers section.
                                                                                                              - The section to jump after a Reset was changed from Core to Status.
                                                                                                              - Improved the way to configure/see the EPCLKs on a global and individual way: now one can go to another section and come back to Clocks
                                                                                                                to still see the individual EPCLKs configuration.
                                                                                                              - Improved the way to configure/see the EPTX and EPRX on the Core section: Now one can go to another section and come back to Core to still
                                                                                                                see the detailed EPRX/EPTX configuration. In addition, one can disable any of the available EPTX/EPRX and still see the changes even when
                                                                                                                going to another section, coming back to Core and pressing detailed view.
                                                                                                              - Changed the naming: Dummy lpGBT is now called Virtual lpGBT. Master lpGBT is now called Cascaded lpGBT(s). The Platform is now called Target.
                                                                                                              - Removed the hardware selection in the Real lpGBT target.
                                                                                                              - Corrected few typos.
    19/11/2021                     `V1.3 <https://cernbox.cern.ch/s/i5WXC9IZT7qJd2o>`_                        - Import and use the `lpgbt_control_lib <https://gitlab.cern.ch/lpgbt/lpgbt_control_lib>`_ in the Backend software.
                                                                                                              - Make the Front-end compatible with both lpGBT V0 and V1.
                                                                                                              - Add FEASTMP enable/disable button.
                                                                                                              - Update documentation in the web application view.
                                                                                                              - Add Watchdog and Timeout in Status.
                                                                                                              - Add configure each clocks globally or individually in Clocks.
                                                                                                              - Improve configuration in Test Features.
    29/06/2020                     `V1.2 <https://cernbox.cern.ch/s/iNiIy3eVV6sW739>`_                        - Add lpGBT CHIPID section in Status.
                                                                                                              - Add Fusing process in Registers.
                                                                                                              - Improve High-Speed equalizer.
                                                                                                              - Improve Eye Diagram in High-Speed.
    28/06/2020                     V1.0/V1.1                                                                  First releases for testing.
   ============================== ========================================================================== =====================================================================================================================================================