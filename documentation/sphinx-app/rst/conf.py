# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'PiGBT Manual'
copyright = u'2022, VLDB+ Team'

version = '0.1'

# The full version, including alpha/beta/rc tags
release = ''


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.todo',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
    # 3rd party extensions
    'localcontrib.fulltoc',
    'cloud_sptheme.ext.table_styling',
    'cloud_sptheme.ext.relbar_links',
    'cloud_sptheme.ext.escaped_samp_literals',
    'cloud_sptheme.ext.issue_tracker',
    'cloud_sptheme.ext.table_styling',
    'cloud_sptheme.ext.role_index',  # NOTE: used only to provide example role index
    #'sphinxcontrib.spelling',
]

spelling_lang='en_US'
spelling_word_list_filename='spelling_wordlist.txt'


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']


# The suffix of source filenames.
source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------


# Add any paths that contain custom themes here, relative to this directory.
html_theme_path = ["../themes/",'../cloud_sptheme/themes']

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = 'cloud'

html_theme = 'sphinx_rtd_theme'
html_theme = 'cloud'

html_theme_options = {}
if 1:#csp.is_cloud_theme(html_theme):
    html_theme_options.update(
        roottarget="index",
        borderless_decor=True,
        sidebarwidth="3in",
        hyphenation_language="en",
    )
    
html_show_sourcelink = False

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
html_title = "PiGBT Manual"



# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
#html_static_path = ['_static']

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
html_last_updated_fmt = '%d/%b/%Y %H:%m'

# If false, no index is generated.
html_use_index = False

html4_writer = True

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
html_show_sphinx = False

# Output file base name for HTML help builder.
htmlhelp_basename = 'PiGBTdoc'


numfig = True


