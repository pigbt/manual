****************
Example features
****************

.. include:: user_stories/clocks_evaluation.rst
.. include:: user_stories/pll_performance_evaluation.rst
.. include:: user_stories/transceiver_configuration.rst
.. include:: user_stories/transmitter_configuration.rst
.. include:: user_stories/analog_peripherals.rst
.. include:: user_stories/highspeed_uplink_evaluation.rst
.. include:: user_stories/highspeed_downlink_evaluation.rst
.. include:: user_stories/raw_registers_access.rst
